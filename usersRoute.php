<?php
namespace App;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
class usersRoute
{
    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\App
     */
    private $app;
    public $jdb;
    public function __construct(Jsondb $jdb) {
        $jdb ->connect();
        $app = new \Slim\App();
        
        $app->get('/', function (Request $request, Response $response) {
            var_dump($jdb);
            $response->getBody()->write("Welcome to the Adroit users Demo.");
            return $response;
        });
        
        $app->group('/users', function ( ) use ($jdb)  {
            //получение списка
            $this->map(['GET'], '', function (Request $request, Response $response) use ($jdb) {
                return $response->withJson(['message' => $jdb ->get('lib', true)]);
            });
            //получение по ид
            $this->get('/{id}', function (Request $request, Response $response, $args) use ($jdb) {
                    $settings = array();
                    $settings['table'] = 'lib';
                    $settings['aid'] = $args['id'];
                    return $response->withJson([$jdb ->get('lib',false,$args['id'])]);

                return $response->withJson(['message' => 'users Not Found'], 404);    
            });

            $this->map(['POST', 'PUT', 'PATCH'], '/{id}', function (Request $request, Response $response, $args) use ($jdb) {
                    $data = $request->getParsedBody();
                    $exinfo['name'] = filter_var($data['name'], FILTER_SANITIZE_STRING);
                    $exinfo['description'] = $data['description'];
                        $settings = array();
                        $settings['table'] = 'lib';
                        $settings['aid'] = $args['id'];
                        $newValue = array();
                        $newValue['name'] = $exinfo['name'];
                        $newValue['description'] = $exinfo['description'];
                        $id = $jdb ->update('lib', $args['id'], $newValue);
                        return $response->withJson([$id['values']['tb_lib'][$args['id']]]);
            });
            //вставка
                    $this->map(['POST', 'PUT', 'PATCH'], '', function (Request $request, Response $response, $args) use ($jdb) {
                    $data = $request->getParsedBody();
                    $exinfo['name'] = filter_var($data['name'], FILTER_SANITIZE_STRING);
                    $exinfo['description'] = $data['description'];
                    if($request->isPost())
                    {

                        $settings = array();
                        $settings['table'] = 'lib';
                        $settings['values'] = array();
                        $settings['values']['name'] = $exinfo['name'];
                        $settings['values']['description'] = $exinfo['description'];
                        $id = $jdb ->insert('lib',$exinfo);
                        return $response->withJson(['message' => "user ".$id." updated successfully"]);
                    }
                    else
                    {
                        return $response->withJson(['message' => 'users Not Found'], 404);  
                    }
                    //return $response->withJson(['message' => 'users Not Found'], 404); 
            });

            $this->delete('/{id}', function (Request $request, Response $response, $args) use ($jdb) {
               // $settings = array();
                //$settings['table'] = 'lib';
                //$settings['aid'] = $args['id'];
                return $response->withJson(['message' => $jdb ->delete('lib',$args['id'] )]); 
            });
                $this->get('/del/{id}', function (Request $request, Response $response, $args) use ($jdb) {
                $settings = array();
                $settings['table'] = 'lib';
                $settings['aid'] = $args['id'];
                return $response->withJson(['message' => $jdb ->delete('lib',$args['id'] )]); 
            });
        });
        
        $this->app = $app;
    }
    /**
     * Get an instance of the application.
     *
     * @return \Slim\App
     */
    public function get()
    {
        return $this->app;
    }
}